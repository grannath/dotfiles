#!/usr/bin/env bash

if [ "$(atq -q p)" != "" ]; then
    # If in work phase, start break
    atq -q p | rg -o "^[0-9]+" | xargs atrm
    ~/.config/polybar/pomodoro-rest.sh
    exit 0
elif [ "$(atq -q r)" != "" ]; then
    # If in break phase, just cancel
    atq -q r | rg -o "^[0-9]+" | xargs atrm
    exit 0
fi

echo "Time to work!" | festival --tts

if [ "$(playerctl status -p playerctld)" != "Playing" ]; then
    playerctl play -p playerctld
fi

echo '~/.config/polybar/pomodoro-rest.sh' | at -q p now + 25 minute
