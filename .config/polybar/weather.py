#!/bin/python

import urllib.request, json

city = "Berlin,de"
api_key = "feb411840b954c31c0d31ebbe827cef9"
units = "metric"
unit_key = "C"
language = "en"

weather = eval(str(urllib.request.urlopen("http://api.openweathermap.org/data/2.5/weather?q={}&APPID={}&units={}&lang={}".format(city, api_key, units, language)).read())[2:-1])

info = weather["weather"][0]["description"].capitalize()
temp = int(float(weather["main"]["temp"]))

print("%s, %i °%s" % (info, temp, unit_key))
