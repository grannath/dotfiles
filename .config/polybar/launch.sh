#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch top and bottom bar on primary display
for m in $(xrandr --query | grep " connected primary" | cut -d" " -f1); do
    MONITOR=$m polybar --reload top &
    MONITOR=$m TRAY=right polybar --reload bottom &
done

sleep 0.5

# Launch top and bottom bar on secondary displays
for m in $(xrandr --query | grep " connected [0-9]" | cut -d" " -f1); do
    MONITOR=$m polybar --reload top &
    MONITOR=$m TRAY='' polybar --reload bottom &
done
