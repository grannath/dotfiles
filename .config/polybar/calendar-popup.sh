#!/bin/sh

YAD_WIDTH=222  # 222 is minimum possible value
YAD_HEIGHT=188 # 188 is minimum possible value
DATE="$(date +"   %Y-%m-%d  %H:%M:%S")"

case "$1" in
--popup)
    if [ "$(xdotool getwindowfocus getwindowname)" = "yad-calendar" ]; then
        exit 0
    fi

    yad --calendar --undecorated --fixed --no-buttons \
        --mouse --close-on-unfocus \
        --width=$YAD_WIDTH --height=$YAD_HEIGHT \
        --title="yad-calendar" >/dev/null &
    ;;
*)
    echo $DATE
    ;;
esac
