#!/usr/bin/env bash

if [ "$(playerctl status -p playerctld)" == "Playing" ]; then
    echo "⏸"
else
    echo "▶"
fi
