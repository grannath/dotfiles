[colors]
black = #1A1A1A
hl_black = #2A2A2A
white = #C4C5B5
hl_white = #F6F6EF
red = #F4005F
green = #98E024
yellow = #FD971F
blue = #9D65FF
magenta = #F4005F
cyan = #58D1EB
orange = #FBA922
turquoise = #16A085

[global/bar-defaults]
font-0 = "Hack Nerd Font Mono:style=Regular:size=10:antialias=true;4"
font-1 = "Material Icons:style=Regular:size=10;3"
font-2 = "Noto Sans Symbols2:style=Regular:size=10;4"
font-3 = "IPAGothic,IPAゴシック:style=Regular:size=10;4"

width = 100%
height = 17

spacing = 1
module-margin = 2

padding-left = 1
padding-right = 1

background = #00000000
foreground = ${colors.white}

line-color = #000000
line-size = 2

[bar/top]
inherit = global/bar-defaults

monitor = ${env:MONITOR:eDP1}
border-bottom-size = 3

modules-left = spotify sp spp sn pomodoro
modules-right = cpu temperature memory filesystem pulseaudio network wireless-network battery

[bar/bottom]
inherit = global/bar-defaults

monitor = ${env:MONITOR:eDP1}
bottom = true
border-top-size = 3

modules-left = i3
modules-center = popup-calendar weather

tray-position = ${env:TRAY:}
tray-padding = 5
tray-transparent = false
;tray-background = #0063ff
tray-maxsize=1000

[module/battery]
type = internal/battery
full-at = 98

format-charging = <animation-charging> <label-charging>
format-discharging = <ramp-capacity> <label-discharging>
format-full = <ramp-capacity> <label-full>

ramp-capacity-0 = 
;ramp-capacity-0-foreground = #f53c3c
ramp-capacity-1 = 
;ramp-capacity-1-foreground = #ffa900
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 

bar-capacity-width = 10
bar-capacity-format = %{+u}%{+o}%fill%%empty%%{-u}%{-o}
bar-capacity-fill = █
;bar-capacity-fill-foreground = #ddffffff
;bar-capacity-fill-font = 3
bar-capacity-empty = █
;bar-capacity-empty-font = 3
;bar-capacity-empty-foreground = #44ffffff

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-framerate = 750

[module/cpu]
type = internal/cpu
interval = 0.5
format = <label> <ramp-coreload>
label = CPU

ramp-coreload-0 = ▁
;ramp-coreload-0-font = 2
ramp-coreload-0-foreground = ${colors.green}
ramp-coreload-1 = ▂
;ramp-coreload-1-font = 2
ramp-coreload-1-foreground = ${colors.green}
ramp-coreload-2 = ▃
;ramp-coreload-2-font = 2
ramp-coreload-2-foreground = ${colors.green}
ramp-coreload-3 = ▄
;ramp-coreload-3-font = 2
ramp-coreload-3-foreground = ${colors.green}
ramp-coreload-4 = ▅
;ramp-coreload-4-font = 2
ramp-coreload-4-foreground = ${colors.orange}
ramp-coreload-5 = ▆
;ramp-coreload-5-font = 2
ramp-coreload-5-foreground = ${colors.orange}
ramp-coreload-6 = ▇
;ramp-coreload-6-font = 2
ramp-coreload-6-foreground = ${colors.red}
ramp-coreload-7 = █
;ramp-coreload-7-font = 2
ramp-coreload-7-foreground = ${colors.red}

[module/popup-calendar]
type = custom/script
exec = ~/.config/polybar/calendar-popup.sh
interval = 1
click-left = ~/.config/polybar/calendar-popup.sh --popup

[module/date]
type = internal/date
date =    %%{F#99}%Y-%m-%d%%{F-}  %%{F#fff}%H:%M:%S%%{F-}
date-alt = %%{F#fff}%A, %d %B %Y  %%{F#fff}%H:%M%%{F#666}:%%{F#fba922}%S%%{F-}

[module/memory]
type = internal/memory
format = <label> <bar-used>
label = RAM

bar-used-width = 15
bar-used-foreground-0 = ${colors.green}
bar-used-foreground-1 = ${colors.green}
bar-used-foreground-2 = ${colors.orange}
bar-used-foreground-3 = ${colors.red}
bar-used-indicator = |
;bar-used-indicator-font = 6
;bar-used-indicator-foreground = #ff
bar-used-fill = ─
;bar-used-fill-font = 6
bar-used-empty = ─
;bar-used-empty-font = 6
bar-used-empty-foreground = ${colors.hl_black}

[module/wireless-network]
type = internal/network
interface = wlp0s20f3
interval = 1.0
ping-interval = 10

format-connected = <ramp-signal> <label-connected>
label-connected = %local_ip%
label-disconnected =   n/c
;label-disconnected-foreground = #66

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 

[module/network]
type = internal/network
interface = enp0s31f6
interval = 1.0
ping-interval = 10

format-connected = LAN: <label-connected>
label-connected = %local_ip%
label-disconnected =   n/c
;label-disconnected-foreground = #66

ramp-signal-0 = 

[module/pulseaudio]
type = internal/pulseaudio

; Sink to be used, if it exists (find using `pacmd list-sinks`, name field)
; If not, uses default sink
sink = alsa_output.usb-JDS_Labs_Inc_C5D_Amp_DAC-01.analog-stereo

; Use PA_VOLUME_UI_MAX (~153%) if true, or PA_VOLUME_NORM (100%) if false
; Default: true
use-ui-max = true

; Interval for volume increase/decrease (in percent points) (unreleased)
; Default: 5
interval = 5

format-volume = <ramp-volume> <bar-volume>
label-muted =   muted
label-muted-foreground = #66

ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 
ramp-volume-3 = 

bar-volume-width = 10
bar-volume-foreground-0 = ${colors.green}
bar-volume-foreground-1 = ${colors.green}
bar-volume-foreground-2 = ${colors.green}
bar-volume-foreground-3 = ${colors.green}
bar-volume-foreground-4 = ${colors.green}
bar-volume-foreground-5 = ${colors.orange}
bar-volume-foreground-6 = ${colors.red}
bar-volume-gradient = false
bar-volume-indicator = |
;bar-volume-indicator-font = 2
bar-volume-fill = ─
;bar-volume-fill-font = 2
bar-volume-empty = ─
;bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.hl_black}

[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

label-mounted =  %free%

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 75

format = <ramp> <label>
;format-underline = #f50a4d
format-warn = <ramp> <label-warn>
;format-warn-underline = ${self.format-underline}

label = %temperature-c%
label-warn = HOT! %temperature-c%
label-warn-foreground = ${colors.red}

ramp-0 = 🌡
;ramp-0 = 🌡
;ramp-0 = 
;ramp-1 = 
;ramp-2 = 
;ramp-foreground = #66

[module/i3]
type = internal/i3

pin-workspaces = true
strip-wsnumbers = true
index-sort = true

; Use fuzzy (partial) matching on labels when assigning
; icons to workspaces
; Example: code;♚ will apply the icon to all workspaces
; containing 'code' in the label
; Default: false
fuzzy-match = true

ws-icon-0 = 1;♚
ws-icon-1 = 2;♛
ws-icon-2 = 3;♜
ws-icon-3 = 4;♝
ws-icon-4 = 5;♞
ws-icon-default = ♟

; Available tags:
;   <label-state> (default) - gets replaced with <label-(focused|unfocused|visible|urgent)>
;   <label-mode> (default)
format = <label-state> <label-mode>

; Available tokens:
;   %mode%
; Default: %mode%
label-mode = %mode%
label-mode-padding = 2
;label-mode-foreground = #16A085

; Available tokens:
;   %name%
;   %icon%
;   %index%
;   %output%
; Default: %icon%  %name%
label-focused =  %index%
label-focused-foreground = ${colors.hl_white}
label-focused-background = ${colors.hl_black}
label-focused-underline = ${colors.turquoise}
label-focused-padding = 3

; Available tokens:
;   %name%
;   %icon%
;   %index%
;   %output%
; Default: %icon%  %name%
label-unfocused = %index%
label-unfocused-padding = 3

; Available tokens:
;   %name%
;   %icon%
;   %index%
;   %output%
; Default: %icon%  %name%
label-visible = %index%
label-visible-foreground = ${colors.hl_white}
label-visible-background = ${colors.hl_black}
;label-visible-underline = ${colors.turquoise}
label-visible-padding = 3

; Available tokens:
;   %name%
;   %icon%
;   %index%
;   %output%
; Default: %icon%  %name%
label-urgent = %index%
label-urgent-foreground = ${colors.black}
label-urgent-background = ${colors.white}
label-urgent-padding = 3

; Separator in between workspaces
label-separator = |
label-separator-padding = 1
label-separator-foreground = ${colors.turquoise}

[module/spotify]
type = custom/script
tail = true
format = <label>
exec = /home/jzick/.config/polybar/spotify-scroll.sh
;exec-if = [[ "$(playerctl status)" = "Playing" || "$(playerctl status)" = "Paused" ]] && echo "0"

format-underline = ${colors.turquoise}

[module/sn]
type = custom/text
content = ""
;content-background = ${colors.one}
click-left = playerctl next -p playerctld

[module/sp]
type = custom/text
content = ""
;content-background = ${colors.one}
click-left = playerctl prev -p playerctld

[module/spp]
type = custom/script
interval = 2
exec = ~/.config/polybar/spotify-playpause.sh
;exec-if = [[ -n "$(playerctl --list-all)" ]] && echo "0"
format = <label>
;format-background = ${colors.one}
click-left = playerctl play-pause -p playerctld

[module/weather]
type = custom/script
interval = 10
format = <label>
format-prefix = "   "
label-padding = 1
exec = ~/.config/polybar/weather.py
format-padding = 1

[module/pomodoro]
type = custom/script
interval = 2
exec = ~/.config/polybar/pomodoro-status.sh
click-left = ~/.config/polybar/pomodoro-startstop.sh
