#!/usr/bin/env bash

diff() {
    TARGET_TIME=`atq -q $1 | rg -o '[0-9]+:[0-9]+:[0-9]+'`
    TARGET_SECONDS=`date +%s -d "$TARGET_TIME"`
    NOW_SECONDS=`date +%s`
    DIFF_SECONDS=`expr $TARGET_SECONDS - $NOW_SECONDS`
    echo `expr $DIFF_SECONDS / 60 + 1`
}

if [ "$(atq -q p)" != "" ]; then
    DIFF_MINUTES=$(diff "p")
    echo "Work time for $DIFF_MINUTES more minutes!"
elif [ "$(atq -q r)" != "" ]; then
    DIFF_MINUTES=$(diff "r")
    echo "Rest for $DIFF_MINUTES more minutes..."
else
    echo "Ready to go!"
fi
