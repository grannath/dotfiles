#!/usr/bin/env bash

if [ "$(playerctl status -p playerctld)" == "Playing" ]; then
    playerctl pause -p playerctld
fi

zenity --info --text "Time for a break!" & (echo "Time for a break!" | festival --tts)

echo 'zenity --info --text "Ready to go!" & (echo "Ready to go!" | festival --tts)' | at -q r now + 5 minute
